package eu.ensg.ign;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class Geometry {
	private float[][] coordinates;
	
	private String txtJson;
	private String url;
	
	
	public Geometry(String url) {
		this.txtJson = HttpClientIgn.request(url);
	}
	
	public void getCoord(){
		String mot = "\"coordinates\":";
		
		int index = this.txtJson.indexOf(mot);
		List<Integer> a = new ArrayList<>();
		while (index >= 0) {
		    a.add(index);
		    index = this.txtJson.indexOf(mot, index + 1);
		}
		
		String word = "},\"attributes\"";
		
		int index2 = this.txtJson.indexOf(word);
		List<Integer> b = new ArrayList<>();
		while (index2 >= 0) {
		    b.add(index2);
		    index2 = this.txtJson.indexOf(word, index2 + 1);
		}
		
		a.remove(0);
		
		int n = 0;
		for(int i = 0; i<a.size(); i++) {
			n+=1;
			String stringArray = txtJson.substring(a.get(i)+14, b.get(i));

		    stringArray = stringArray.substring(1, stringArray.length() - 1);

		    Pattern pattern = Pattern.compile("\\[(.*?)\\]");
		    Matcher matcher = pattern.matcher(stringArray);

		    
		    int count = StringUtils.countMatches(stringArray, ",");
		    
		    
		    float[][] floatArray = new float[(count+1)/2][2];
		    int k = 0;
		    int j = 0;
		    while (matcher.find()){
		        String group = matcher.group(1);
		        String[] splitGroup = group.split(",");
		        for (String s : splitGroup){
		            floatArray[k][j] = Float.valueOf(s.trim());
		            j++;
		        }
		        j = 0;
		        k++;
		    }
		    System.out.println("Les coordonnées des steps de la portion " + n +  " " + Arrays.deepToString(floatArray));
		    
		}
	}
	
	public float[][] getCoord0(){
		String mot = "\"coordinates\":";
		
		int index = this.txtJson.indexOf(mot);
		List<Integer> a = new ArrayList<>();
		while (index >= 0) {
		    a.add(index);
		    index = this.txtJson.indexOf(mot, index + 1);
		}
		
		String word = "},\"attributes\"";
		
		int index2 = this.txtJson.indexOf(word);
		List<Integer> b = new ArrayList<>();
		while (index2 >= 0) {
		    b.add(index2);
		    index2 = this.txtJson.indexOf(word, index2 + 1);
		}
		
		a.remove(0);
		
		int n = 0;
		
		float[][] coord = {};
		
		
		for(int i = 0; i<a.size(); i++) {
			n+=1;
			String stringArray = this.txtJson.substring(a.get(i)+14, b.get(i));

		    stringArray = stringArray.substring(1, stringArray.length() - 1);

		    Pattern pattern = Pattern.compile("\\[(.*?)\\]");
		    Matcher matcher = pattern.matcher(stringArray);

		    
		    int count = StringUtils.countMatches(stringArray, ",");
		    
		    
		    float[][] floatArray = new float[(count+1)/2][2];
		    int k = 0;
		    int j = 0;
		    while (matcher.find()){
		        String group = matcher.group(1);
		        String[] splitGroup = group.split(",");
		        for (String s : splitGroup){
		            floatArray[k][j] = Float.valueOf(s.trim());
		            j++;
		        }
		        j = 0;
		        k++;
		    }
		    coord = addX(n,coord,floatArray[0]);
		}
		coord = ArrayUtils.remove(coord, 0);
		return coord;
	}
	
	public float[][] getCoordN(){
		String mot = "\"coordinates\":";
		
		int index = this.txtJson.indexOf(mot);
		List<Integer> a = new ArrayList<>();
		while (index >= 0) {
		    a.add(index);
		    index = this.txtJson.indexOf(mot, index + 1);
		}
		
		String word = "},\"attributes\"";
		
		int index2 = this.txtJson.indexOf(word);
		List<Integer> b = new ArrayList<>();
		while (index2 >= 0) {
		    b.add(index2);
		    index2 = this.txtJson.indexOf(word, index2 + 1);
		}
		
		a.remove(0);
		
		int n = 0;
		
		float[][] coord = {};
		
		
		for(int i = 0; i<a.size(); i++) {
			n+=1;
			String stringArray = this.txtJson.substring(a.get(i)+14, b.get(i));

		    stringArray = stringArray.substring(1, stringArray.length() - 1);

		    Pattern pattern = Pattern.compile("\\[(.*?)\\]");
		    Matcher matcher = pattern.matcher(stringArray);

		    
		    int count = StringUtils.countMatches(stringArray, ",");
		    
		    
		    float[][] floatArray = new float[(count+1)/2][2];
		    int k = 0;
		    int j = 0;
		    while (matcher.find()){
		        String group = matcher.group(1);
		        String[] splitGroup = group.split(",");
		        for (String s : splitGroup){
		            floatArray[k][j] = Float.valueOf(s.trim());
		            j++;
		        }
		        j = 0;
		        k++;
		    }
		    coord = addX(n,coord,floatArray[floatArray.length-1]);
		}
		coord = ArrayUtils.remove(coord, 0);
		return coord;
	}
	public static float[][] addX(int n, float[][] coord, float[] floatArray)
    {
        int i;
  
        // create a new array of size n+1
        float newarr[][] = new float[n + 1][2];
  
        // insert the elements from
        // the old array into the new array
        // insert all elements till n
        // then insert x at n+1
        for (i = 1; i < n; i++)
            newarr[i] = coord[i];
  
        newarr[n] = floatArray;
  
        return newarr;
    }
	
	
	
	
	@Override
	
	public String toString() {
		this.getCoord();
		return "";
	}
}