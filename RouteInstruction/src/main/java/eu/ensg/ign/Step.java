package eu.ensg.ign;


public class Step {
	
	private double distance;
	
	public double getDistance() {
		return this.distance;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	private double duration;
	
	public double getDuration() {
		return this.duration;
	}
	
	public void setDuration(double duration) {
		this.duration = duration;
	}
	
	private Geometry geom;
	
	public Geometry getGeometry() {
		return this.geom;
	}
	
	public void setGeometry(Geometry geom) {
		this.geom = geom;
	}
	

}
