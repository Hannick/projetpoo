package eu.ensg.ign;

import java.util.List;

public class Portion {

	private double distance;
	public double getDistance() { return this.distance; }
	public void setDistance(double distance) { this.distance = distance; }
	
	private String start;
	public String getStart() { return this.start; }
	public void setStart(String start) { this.start = start; }

	private List<Step> steps;
	public List<Step> getSteps() { return this.steps; }
	public void setPortions(List<Step> steps) { this.steps = steps; }
	
	private String end;
	public String getEnd() { return this.end; }
	public void setEnd(String end) { this.end = end; }
	

}
