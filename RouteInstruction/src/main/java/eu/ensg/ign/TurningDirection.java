package eu.ensg.ign;

import java.util.ArrayList;
import java.util.List;

import eu.ensg.osm.Point;

public class TurningDirection {
	
	
	public static List<Double> direction(Geometry geom, int nbPortions) {
		float[][] ListStepDebut = geom.getCoord0();
		float[][] ListStepFin = geom.getCoordN();
		List<String> directions = new ArrayList<>();
		List<Double> angles = new ArrayList<>();
		for(int k = 0; k<nbPortions-1; k++) {
			if(!(ListStepDebut[k].equals(ListStepFin[k]))) {
				
				Point P1 = new Point(ListStepDebut[k][0],ListStepDebut[k][1]);
				Point P2 = new Point(ListStepFin[k][0],ListStepFin[k][1]);
				Point P3 = new Point(ListStepFin[k+1][0],ListStepFin[k+1][1]);
				
				
				double c = P1.haversine(P3);
				double b = P1.haversine(P2);
				double a = P2.haversine(P3);
				double cosalpha = (c*c - a*a - b*b)/(-2*a*b);
				double alpharad = Math.acos(cosalpha);
				double alpha = Math.toDegrees(alpharad);
				
				angles.add(alpha);
				
				if(-45<alpha && alpha<45) {
					directions.add("à droite");
				}
				else if(45<alpha && alpha<135) {
					directions.add("tout droit");
				}
				else if(-135<alpha && alpha<135) {
					directions.add("à gauche");
				}
				else {
				}
			}
			
		}
		
		return angles;
		
	}
}
