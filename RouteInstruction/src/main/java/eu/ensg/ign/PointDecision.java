package eu.ensg.ign;

public class PointDecision {
	double lon;
	double lat;
	
	public PointDecision(float[] step0) {
		this.lon = step0[0];
		this.lat = step0[1];
	}
	
	public double getLon() {
		return this.lon;
	}
	
	public double getLat() {
		return this.lat;
	}
	
	@Override
	
	public String toString() {
		return this.lon +" -- "+this.lat;
	}

}
