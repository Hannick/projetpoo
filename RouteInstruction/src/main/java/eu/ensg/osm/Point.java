package eu.ensg.osm;

public class Point {
	public double lon;
	public double lat;

	public Point(double lon, double lat){
		this.lon = lon;
		this.lat = lat;
	}

	static double toRad(double angdeg) {
		return Math.toRadians(angdeg);
	}
	
	public double haversine(Point point) { 
		
		//  
		double dLat = toRad(point.lat - this.lat); 
		double dLon = toRad(point.lon - this.lon); 

		// convertit en radians les latitudes 
		double latrad = toRad(this.lat); 
		double pointlatrad = toRad(point.lat); 

		// On applique la formule
		double a = Math.pow(Math.sin(dLat / 2), 2) +  Math.pow(Math.sin(dLon / 2), 2) * Math.cos(latrad) * Math.cos(pointlatrad); 
		double c = 2 * Math.asin(Math.sqrt(a));
		double rad = 6371;
		
		return rad * c;
	} 
	
	public String toString() {
		return this.lon + "--" + this.lat;
	}
	
}
