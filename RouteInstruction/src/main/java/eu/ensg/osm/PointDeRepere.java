package eu.ensg.osm;

public class PointDeRepere  {
	private String nom;
	private double lon;
	private double lat;
	
	public PointDeRepere(double lon, double lat) {
		this.lon = lon;
		this.lat = lat;
		this.nom = "pas utile pour l'instant";
	}
	
	public PointDeRepere(double lon, double lat,String nom){
		this.lon = lon;
		this.lat = lat;
		this.nom = nom;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public double getLon() {
		return this.lon;
	}
	
	public double getLat() {
		return this.lat;
	}
	
	@Override
	
	public String toString() {
		return this.nom + " : " + this.lon + " -- " + this.lat;
	}
}
