package eu.ensg.osm;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class RecupPointRepere {
	
	public static List<PointDeRepere> recup(ZoneTampon bbox) throws ParserConfigurationException{
		List<PointDeRepere> P = new ArrayList<>();
		String dataRequest = "<osm-script>"
				+ "<union>"
				+ "<query type=\"node\">"
				+ "<bbox-query e=\"" + bbox.getE() + "\" n=\"" + bbox.getN() + "\" s=\"" + bbox.getS() + "\" w=\"" + bbox.getO() + "\" />"
				+ "</query>"
				+ "</union>"
				+ "<print mode=\"meta\"/>"
				+ "</osm-script>";
		String xmldata = HttpClientOsm.getOsmXML(dataRequest);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		try {
			Document doc = builder.parse(new ByteArrayInputStream(xmldata.getBytes()));
			doc.getDocumentElement().normalize();
		    Element root = (Element) doc.getElementsByTagName("osm").item(0);
		
		    int nbNoeuds = root.getElementsByTagName("node").getLength();
		    for (int i = 0; i < nbNoeuds; i++) {

		    	Element elem = (Element) root.getElementsByTagName("node").item(i);

		    	// On récupère son ID
		    	long id = Long.valueOf(elem.getAttribute("id"));

		    	// on récupère sa géométrie
		    	double lat = Double.valueOf(elem.getAttribute("lat"));
		    	double lon = Double.valueOf(elem.getAttribute("lon"));
		    	
		    	
		    	
		    	for (int j = 0; j < elem.getElementsByTagName("tag").getLength(); j++) {
					Element tagElem = (Element) elem.getElementsByTagName("tag").item(j);
					String cle = tagElem.getAttribute("k");
					String val = tagElem.getAttribute("v");
					
					if (cle.equals("name")) {
						//System.out.println(cle + "--" + val);
						//System.out.println(lon + "," + lat);
						PointDeRepere p = new PointDeRepere(lon,lat,val);
						P.add(p);
					}
						
		    	}
		    	
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return P;
	}
	
	

}
