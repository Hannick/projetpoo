package eu.ensg.osm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ZoneTampon {
	
	private double lon;
	private double lat;
	private static final double r = 6371 ;
	private double E;
	private double N;
	private double O;
	private double S;
	private List<Double> Coordonnees = new ArrayList<>();
	
	static double toRad(double angdeg) {
		return Math.toRadians(angdeg);
	}
	
	public ZoneTampon(double E, double N, double O, double S) {
		this.E = E;
		this.N = N;
		this.O = O;
		this.S = S;
	}
	
	public ZoneTampon(double lon,double lat) {
		this.lon=lon;
		this.lat=lat;
		double DeltaPhi=2*Math.asin(Math.sin(70/(2*r)));
		double latrad = toRad(this.lat);
		double DeltaLambda=2*Math.asin(Math.sqrt((Math.pow(Math.sin(70/(2*r)),2)/Math.pow(Math.cos(latrad),2))));
		
		E = this.lon + DeltaLambda;
		N = this.lat + DeltaPhi;
		O = this.lon - DeltaLambda;
		S = this.lat - DeltaPhi;
		
		Collections.addAll(this.Coordonnees,E,N,O,S);
	}
	
	public double getE() {
		return this.E;
	}
	public double getN() {
		return this.N;
	}
	public double getO() {
		return this.O;
	}
	public double getS() {
		return this.S;
	}
	
	
	@Override
	
	public String toString() {
		return this.Coordonnees.toString();
	}
	
	public static void main(String[] args) {
		double lonex = 2.5877388;
		double latex = 48.8407423;
		ZoneTampon bbox = new ZoneTampon(lonex,latex);
		
		System.out.println(bbox.toString());
	}
	

}
