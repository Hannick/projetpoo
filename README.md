# PROJET EN PROGRAMMATION JAVA 

### Sujet : CALCUL DES INSTRUCTIONS DE NAVIGATION POUR UN ITINERAIRE A PIED

 

![i](logoecole.png)

## Elèves ING1 ##


HANNICK ABDUL KUTHOOS, ALEXYS REN, NOÉMIE BRICOUT 


## MANULE D'UTILISATION DE L'APPLICATION ##

### Ecran d'accueil ###

Sur l'écran d'accueil, il y a deux cases : 
- [ ] "Start" --> dans cette section il faut entrer les coordonnées géographiques (première case longitude et deuxième case latitude) du point de départ du trajet
- [ ] "End" --> dans cette section il faut entrer les coordonnées géographiques (première case longitude et deuxième case latitude) du point de départ du trajet

<details><summary>Il faut ensuite appuyer sur le bouton "GO"</summary> Ce bouton sert pour lancer le calcul de l'itinéraire et lancer l'application

</details>
:warning: Il faut appuyer sur la touche entrée après chaque entrée de coordonnées

### Carte résultante et itinéraire "pas à pas" ###

Après les calculs, l'écran positionne la carte centrée sur le point de départ (le point rouge sur la carte). Le cadre, sur le bas de l'écran, contient les instructions de déplacement, une à la fois. Pour passer à l'instruction suivante et au point suivant (ou les instruction et points précédents), il faut utiliser les flèches en bas à droite de l'écran.

Il ne faut pas déplacer la carte car elle est directement centrée sur les coordonnées de chaque étape. En cas d'accident, il suffit de faire un aller-retour sur les instructions et la carte se replacera d'elle même sur le bon point.









***

## MANUEL D'INSTALLATION DE L'APPLICATION ##

 


















